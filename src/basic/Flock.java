package basic;

import java.util.ArrayList;
import java.util.Random;
import rules.Rule;

public class Flock {

	private ArrayList<Human> flock;
	private ArrayList<Rule> rules;
	private static Random random = new Random();
	private Layer playground;
	private Cell[][] playfield;

	public Flock(int flockSize, Layer l, Cell[][] f) {
		this.playground = l;
		this.playfield = f;
		flock = new ArrayList<Human>();
		rules = new ArrayList<Rule>();
		for (int i = 0; i < flockSize; i++) {
			addHumans();
		}
	}

	private void addHumans() {
		// Layer layer = playground;

		// random location
		double x = random.nextDouble() * playground.getWidth();
		double y = random.nextDouble() * playground.getHeight();

		// dimension
		double width = 5; // 50 for triangle
		double height = 5; // width / 2.0;

		// create human data
		Vector2D location = new Vector2D(x, y);
		Vector2D velocity = new Vector2D(0, 0);
		Vector2D acceleration = new Vector2D(0, 0);

		// create sprite and add to layer
		Human human = new Human(playground, location, velocity, acceleration, width, height);

		// register human
		flock.add(human);
	}

	public void update() {
		// long startTime = System.nanoTime();
		/*
		 * for(int i=0; i<cells.size(); ++i) { Cell current = cells.get(i);
		 * ArrayList<Cell> borderCells = new ArrayList<Cell>(); final int sizeX = 9999;
		 * for(int x=-1; x<2; x++) { for(int y=-1; y<2; y++) { if(x==0 && y==0)
		 * continue; int pos = x + sizeX*y; pos += i; borderCells.add(cells.get(pos)); }
		 * }
		 * 
		 * }
		 */

		// add/remove Human to/from Cell in order of their position
		manageCells();

		// go through each cell and get Humans of Cell
		for (int x = 0; x < playfield.length; x++) {
			for (int y = 0; y < playfield[0].length; y++) {
				Cell current = playfield[x][y];
				current.setVisited(true);
				ArrayList<Human> currentHumans = new ArrayList<Human>();
				ArrayList<Human> nearHumans = new ArrayList<Human>();
				currentHumans = current.getHumans();
				for (int i = -1; i < 2; i++) {
					for (int j = -1; j < 2; j++) {
						int posX = x + i;
						int posY = y + j;
						if (posX >= 0 && posY >= 0 && posX < playfield.length && posY < playfield[0].length) {
							Cell aroundCells = playfield[posX][posY];
							nearHumans.addAll(aroundCells.getHumans());
						}
					}
				}
				for (Human h : currentHumans) {
					calculateVelocity(h, nearHumans);
				}
			}
		}

		// display humans
		for (Human h : flock) {
			h.move();
			h.display();
		}
		// long endTime = System.nanoTime();
		// long duration = (endTime - startTime);
		// System.out.println(duration);
	}

	private void manageCells() {
		for (int x = 0; x < playfield.length; x++) {
			for (int y = 0; y < playfield[0].length; y++) {
				Cell c = playfield[x][y];
				for (Human h : flock) {
					if (c.has(h.getLocation())) {
						if (!c.getHumans().contains(h)) {
							c.addHuman(h);
							// System.out.println("now Add");
						}
					} else if (c.getHumans().contains(h)) {
						if (c.getHumans().size() > 0)
							c.removeHuman(h);
						// System.out.println("now Remove");
					}
				}
			}
		}
	}

	/*
	 * private void limiSpeed(Human h) { // TODO Auto-generated method stub
	 * 
	 * }
	 */

	private void calculateVelocity(Human h, ArrayList<Human> nearHumans) {
		Vector2D change = new Vector2D(0, 0);
		for (Rule r : rules) {
			change.add(r.getChangeVector(h, nearHumans));
		}
		h.addVelocity(change);
	}

	public void addRule(Rule r) {
		rules.add(r);
	}

	public void removeRule(Rule r) {
		rules.remove(r);
	}
	
	public ArrayList<Human> getFlock() {
		return flock;
	}
}
